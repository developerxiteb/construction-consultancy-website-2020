<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'stratgj7_construction' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ppn[xW%)y12E]05W2`D%$_3x>tcl[L[6!fx*Qa!ixY<g b83@Bw!vIE?1[y>;d]!' );
define( 'SECURE_AUTH_KEY',  'e/2(!]K_l[8<THrUbQuHO-Cx3(8O[&rX|#M>H6 OG,Us688W+df!igt*$ Z}9= =' );
define( 'LOGGED_IN_KEY',    'e.`uy8#U?lcJ]~~f2eV2XWpw]dD.y|Adkg4 Xn-Lxy6`mr4N129m.<<8k=o))4eg' );
define( 'NONCE_KEY',        ']?ijdMJwLomb_<YITBb(]*Yy81p@)O`vBKOK:iwy8[3}?7@4r`A>bOv-zYP{):6D' );
define( 'AUTH_SALT',        'T};0}.G8t(]uipJ6{<z,&TS<H,jltEvev_orR?W252fHMXJT3U N@0gk{2,Z0`B/' );
define( 'SECURE_AUTH_SALT', 'Bp8vHi,gRyiOz9nOM,OFNc^**D;y0d|gDG(ok=f2=`?<5R1W`ehiK>/I$CLF8-@c' );
define( 'LOGGED_IN_SALT',   'V5dQLO#C7~`yfm|V~OTGhR)@T{cZfRdu3N+}_9x}d$xK{_[J >S `b{S+!9Z<nc9' );
define( 'NONCE_SALT',       '_i/^^V{e|$D^-!0-:r?[DeYRK=ufp?[wiiS?V7&HXH)1)8&omzb)VC*5:AMr4p<U' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
