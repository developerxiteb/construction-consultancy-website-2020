<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'stratgj7_WP1SR');

/** MySQL database username */
define('DB_USER', 'stratgj7_WP1SR');

/** MySQL database password */
define('DB_PASSWORD', 'yX5FwT33ji4pQp2ib');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '96727ca4a19c9bf4b827f900175bcd3688cf8fe003c22449e8a51731915391ec');
define('SECURE_AUTH_KEY', '3a35a271692b70739f6bb037aedcbfa48ec6b96e83e2141a5aec0201baf74614');
define('LOGGED_IN_KEY', '3df080646290ee5b3867dfadfc693132bd0d28f8ace86b501343642a7cf44cd4');
define('NONCE_KEY', '2fa52290184b42b6a7bdf9b0e9248e970b8fdb5190a37e56b35143da8fbe7fbe');
define('AUTH_SALT', '792c5f130625293097cc6c8430ff6b54a06f7689512d649009c67fd781bbf9ca');
define('SECURE_AUTH_SALT', 'a1879a901826a081b0f1e250272ee9ea9f7edc259f204a1919976acd369f6aea');
define('LOGGED_IN_SALT', '39fc249fa2f2ca47c9ed3fc2a702076118266894beab8a7c39142b91acc3ddcb');
define('NONCE_SALT', '349317ef96e4181ed465a5f949bfc8b69881e54c39f962e5b0b0c154dc8797f3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = '_1SR_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define( 'WP_CRON_LOCK_TIMEOUT', 120   ); 
define( 'AUTOSAVE_INTERVAL',    300   );
define( 'WP_POST_REVISIONS',    5     );
define( 'EMPTY_TRASH_DAYS',     7     );
define( 'WP_AUTO_UPDATE_CORE',  true  );


