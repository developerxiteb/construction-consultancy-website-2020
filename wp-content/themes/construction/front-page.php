<?php /* Template Name: Home Page */ ?>
<!DOCTYPE html>
<html lang='en'>
<head>
    <?php get_header();
?>
    <?php wp_head();
?>
</head>
<body>
    <!-- Preloader Part Start -->
    <div class='preload-main'>
        <div class='preloader'>
            <div id='nest1'></div>
        </div>
    </div>
    <!-- Preloader Part End -->
    <!-- HEADER AREA START -->
    <nav class='navbar navbar-expand-lg navbar-light bg-light sticky-top'>
        <div class='container-fluid'>
            <img class="svk_logo" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="logo">
            <a class='navbar-brand' href='<?php echo get_home_url(); ?>'>Developing Synergy</a>
            <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent'
                aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
                <i class='fa fa-bars' aria-hidden='true'></i>
            </button>
            <div class='collapse navbar-collapse menu-main' id='navbarSupportedContent'>
                <ul class='navbar-nav ml-auto menu-item2'>
                    <li class='nav-item'>
                        <a class='nav-link' href='#banner'>Home</a>
                    </li>
                    <li class='nav-item'>
                        <a class='nav-link' href='#overview'>Who We Are</a>
                    </li>
                    <li class='nav-item'>
                        <a class='nav-link' href='#about'>Service Portfolio</a>
                    </li>
                    <li class='nav-item'>
                        <a class='nav-link' href='#synerg'>Synergistic Action</a>
                    </li>
                    <li class='nav-item'>
                        <a class='nav-link' href='#project'>Gallery</a>
                    </li>
                    <li class='nav-item'>
                        <a class='nav-link' href='#contact'>Contact Us</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- HEADER AREA END -->
    <!-- BANNER AREA START -->
    <section id='banner'>
        <div class='arrows'>
            <i class='fa fa-long-arrow-left slidPrv' aria-hidden='true'></i>
            <i class='fa fa-long-arrow-right slidNext' aria-hidden='true'></i>
        </div>
        <div class='banner-main'>
            <?php if ( have_rows( 'slider' ) ): ?>
            <?php while( have_rows( 'slider' ) ): the_row();?>
            <div class='banner-item' style="background: url(<?php the_sub_field('slider_image'); ?>);">
                <div class='container zindex'>
                    <div class='row m-0'>
                        <div class='col-lg-7 banner-text'>
                            <h1><?php the_sub_field( 'slider_title' );?></h1>
                            <h2><?php the_sub_field( 'main_title' );?></h2>
                            <p><?php the_sub_field( 'slider_description' );?></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile;?>
            <?php endif;?>
        </div>
    </section>
    <!-- BANNER AREA END -->
    <!-- OVERVIEW AREA START -->
    <section id='overview'>
        <div class='container'>
            <div class='row m-0'>
                <div class='col-lg-6 over-text'>
                    <img src='<?php echo get_template_directory_uri(); ?>/images/offericon.png' alt='offer-icon'>
                    <h3>Who We Are</h3>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
                    <?php the_content();?>
                    <?php endwhile; endif;?>
                </div>
                <?php if ( have_rows( 'who_we_are_image' ) ): ?>
                <?php while( have_rows( 'who_we_are_image' ) ): the_row();?>
                <div class='col-lg-3 col-sm-6 ' style="display:flex;align-items:center;justify-content:center;">
                    <div class='over-main d-flex align-center'>
                        <div class='om-img'>
                            <img src="<?php the_sub_field('image_about'); ?>" alt='overview-img'
                                class='img-fluid w-100'>
                        </div>
                        <div class='om-text'>
                            <h3><?php the_sub_field( 'about_title' );?></h3>
                            <p><?php the_sub_field( 'about_sub_title' );?></p>
                        </div>
                    </div>
                </div>
                <?php endwhile;?>
                <?php endif;?>
            </div>
        </div>
    </section>
    <!-- OVERVIEW AREA END -->
    <!-- SERVICE PORTFOLIO AREA START -->
    <section id='about'>
        <div class='backtotop'>
            <a href='#banner'><i class='fa fa-angle-up' aria-hidden='true'></i></a>
        </div>
        <div class='container'>
            <div class='row m-0'>
                <div class='col-lg-12 about-heading'>
                    <h3>SERVICE PORTFOLIO</h3>
                </div>
            </div>
        </div>
        <div class='container'>
            <div class='row m-0 padding-top'>
                <div class='col-lg-12'>
                    <div class='about-img'>
                        <div class='video-icon'>
                            <a class='venobox' data-autoplay='true' data-vbtype='video' href="<?php the_field('portfolio_video_url'); ?>">
                                <!-- <i class='fa fa-play' aria-hidden='true'></i> -->
                            </a>
                        </div>
                        <img src='<?php echo get_template_directory_uri(); ?>/images/abt2.jpeg' alt='about-img'
                            class='img-fluid'>
                    </div>
                </div>
                <div class="row m-0 mt-3 p-3">
                        <?php the_field( 'portfolio_text' );?>
                </div>
            </div>
        </div>
    </section>
    <!-- ABOUT AREA END -->
    <!-- SERVICE AREA START -->
    <section id='synerg' class='mt-4'>
        <div class='container'>
            <div class='row m-0'>
                <div class='col-lg-12 about-heading'>
                    <h3>SYNERGISTIC
                            ACTION IN
                            ECONOMY</h3>
                </div>
            </div>
            <div class='row m-0 padding-top3'>
                <div class='col-lg-12 col-sm-12 col-md-12 m-sm-auto'>
                    <div class='serv-item extra-mar ser-tab-mar text-center'>
                        <!-- <div class='serv-img'>
                            <img src='<?php the_field( 'synergistic' );?>' alt='service-icon'>
                        </div> -->
                        <img class="img-fluid" src='<?php the_field( 'synergistic' );?>' alt='service-icon'>
                        <?php the_field( 'synergistic_action_content' );?>
                    </div>
                </div>
                <div class='col-lg-12 col-sm-12 col-md-12 m-sm-auto'>
                    <div class='serv-item extra-mar ser-tab-mar text-center'>
                        <!-- <div class='serv-img'>
                            <img src='<?php the_field( 'synergistic_action_icon_two' );?>' alt='service-icon'>
                        </div> -->
                        <img src='<?php the_field( 'synergistic_action_icon_two' );?>' alt='service-icon'>
                        <?php the_field( 'synergistic_action_content_two' );?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- SERVICE AREA END -->
    <!-- PROJECT AREA START -->
    <section id='project'>
        <div class='container'>
            <div class='row m-0'>
                <div class='col-lg-12 about-heading head-wh'>
                    <h3>GALLERY</h3>
                </div>
            </div>
        </div>
    </section>
    <div id='portfolio' class='padding-top'>
        <div class='container'>
            <div class='row m-0'>
                <div class='portfolioFilter clearfix col-lg-12'>
                    <a href='#' data-filter='*' class='current'>All</a>
                    <?php
$categories = get_categories( 'parent=2' );
if ( isset( $categories ) && !empty( $categories ) ) {
    foreach ( $categories as $category ) { ?>
        <a href='#' data-filter='.<?php echo $category->slug; ?>'><?php echo $category->name;
        ?>
        </a>
                    <?php }
    } ?>
                </div>
            </div>
            <div class='row m-0 portfolioContainer'>
                <?php
    $cats = get_categories( 'parent=2' );
    // loop through the categries
    foreach ( $cats as $cat ) {
        // setup the cateogory ID
        $cat_id = $cat->term_id;
        query_posts( "cat=$cat_id&posts_per_page=100" );
        if ( have_posts() ) : while ( have_posts() ) : the_post();
        $featured_img_url = get_the_post_thumbnail_url( get_the_ID() );
        //     echo $featured_img_url;
        ?>
                <div class='<?php echo $cat->slug; ?> objects logos ui col-lg-4 col-sm-6 col-md-4'>
                    <div class='port-item'>
                        <img src='<?php  echo $featured_img_url;?>' alt='image' class='img-fluid w-100'>
                        <div class='filter-overlay'>
                            <a href='<?php  echo $featured_img_url;?>' data-lightbox='roadtrip'
                                data-title='Project 01'><i class='fa fa-plus' aria-hidden='true'></i>
                            </a>
                        </div>
                    </div>
                </div>
                <?php endwhile;
wp_reset_postdata();
          endif;
    }
    ?>
<?php wp_reset_query(); ?>
            </div>
        </div>
    </div>
    <!-- PROJECT AREA END -->
    <!-- TEAM AREA START -->
    <!-- TEAM AREA END -->
    <!-- CONTACT AREA START -->
    <section id='contact'>
        <div class='container zindex'>
            <div class='row m-0'>
                <div class='col-lg-12 about-heading head-wh'>
                    <h3>CONTACT</h3>
                </div>
            </div>
            <div class='row m-0 padding-top'>
                <div class='col-lg-10 m-auto'>
					
                    <?php echo do_shortcode('[contact-form-7 id="244" title="Contact form 1"]'); ?>
                </div>
            </div>
        </div>
    </section>
    <!-- CONTACT AREA END -->
    <!-- MARKET AREA START -->
    <div id='market'>
        <div class='container'>
            <div class='row m-0'>
                <div style="color:black;" class='col-lg-12 about-heading head-wh'>
                    <h3 style="color:black;margin-bottom:20px;">OUR PARTNERS</h3>
                </div>
            </div>
            <div class='row m-0'>
                <div class='col-lg-12'>
                    <div class='market-main'>
                        <?php if ( have_rows( 'logolist' ) ): ?>
                        <?php 
                        while( have_rows( 'logolist' ) ): the_row();
    ?>
                        <div class='col-lg-3'>
                            <div class='market-item text-center'>
                                <img src="<?php the_sub_field('logoimage'); ?>" alt='company-img' class='img-fluid'>
                            </div>
                        </div>
                        <?php endwhile;  endif;
    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- MARKET AREA END -->
    <!-- FOOTER AREA START -->
    <footer id='footer'>
        <div class='container'>
            <div class='row m-0'>
                <div class='col-lg-4 col-md-6 col-sm-12'>
                    <div class='footer-logo'>
                        <?php if ( is_active_sidebar( 'aboutarea' ) ) : ?>
                        <?php dynamic_sidebar( 'aboutarea' );
                            ?>
                        <?php endif;
                                ?>
                    </div>
                </div>
                <div class='col-lg-2 col-sm-6 col-sm-12'>
                    <div class='links'>
                        <h3>Navigation</h3>
                        <?php
    wp_nav_menu( array( 'theme_location' => 'footer_bottom', 'container_class' => '', 'menu_class' => 'list-inline',
    'menu_id' => '', ) );
    ?>
                    </div>
                </div>
                <div class='col-lg-3 col-sm-6'>
                    <div class='location links'>
                        <?php if ( is_active_sidebar( 'address' ) ) : ?>
                        <?php dynamic_sidebar( 'address' );
    ?>
                        <?php endif;
    ?>
                    </div>
                </div>
                <div class='col-lg-3 col-sm-6'>
                    <div class='location links'>
                        <?php if ( is_active_sidebar( 'connect' ) ) : ?>
                        <?php dynamic_sidebar( 'connect' );
    ?>
                        <?php endif;
    ?>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- FOOTER AREA END -->
    <!-- COPY_RIGHT AREA START -->
    <section id='footer-btm'>
        <div class='container'>
            <div class='row m-0'>
                <div class='col-lg-12 moja-loss'>
                    <div class='row m-0'>
                        <div class='col-lg-6'>
                            <div class='fop-btm'>
                                <h2>Copyright &copy;
                                    2020. All rights reserved by <a class="ftr_com_name"
                                        href='<?php echo get_home_url(); ?>'>Strategic Support</a></h2>
                                        <img class="ftr_logo" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="footer logo">
                            </div>
                        </div>
                        <div class='col-lg-6 text-center'>
                            <div class="row m-0">
                                <div class='footer-social newfs col-lg-6'>
                                    <a href='#'><i class='fa fa-facebook' aria-hidden='true'></i></a>
                                    <a href='#'><i class='fa fa-twitter' aria-hidden='true'></i></a>
                                    <a href='#'><i class='fa fa-google-plus' aria-hidden='true'></i></a>
                                    <a href='#'><i class='fa fa-dribbble' aria-hidden='true'></i></a>
                                    <a href='#'><i class='fa fa-linkedin' aria-hidden='true'></i></a>
                                </div>
                                <div class='fop-btm col-lg-6'>
                                    <h2>Site By<a target="_blank"
                                            href='http://www.xiteb.com'>&nbsp;Xiteb</a></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- COPY_RIGHT AREA END -->
    <!-- Optional JavaScript -->
    <?php get_footer();
    ?>
    <?php wp_footer();
    ?>
</body>
</html>