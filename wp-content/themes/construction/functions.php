<?php


add_theme_support('post-thumbnails');
function spa_widgets_init() {
    register_sidebar(array(
        'name' => 'About Info',
        'id' => 'aboutarea',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '  <a class="f-logo" href="#banner"><b>',
        'after_title' => '</b></a>',



      

    ));
    
     register_sidebar(array(
        'name' => 'Address',
        'id' => 'address',
        'before_widget' => '',
        'after_widget' => '',
//        'before_title' => '<h2 class="title-wh">',
//        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => 'Connect',
        'id' => 'connect',
        'before_widget' => '',
        'after_widget' => '',
//        'before_title' => '<h2 class="title-wh">',
//        'after_title' => '</h2>',
    ));

}
remove_filter('widget_text_content', 'wpautop');
add_action('widgets_init', 'spa_widgets_init');



function register_my_menus() {
    register_nav_menus(
            array(
                'mainmenu' => __('Main Menu'),
                'footer_bottom' => __('Footer menu')
            )
    );
}

add_action('init', 'register_my_menus');